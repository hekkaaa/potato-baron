﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Database.BlackBox
{
    internal class CreateUniqueKey
    {   

        public static string CreateKey()
        {
            byte[] bytes = new byte[16]; // байтовая длина.
            RandomNumberGenerator rng = RandomNumberGenerator.Create(); //  Создает экземпляр реализации по умолчанию криптографического генератора случайных чисел, позволяющего генерировать случайные данные.
            rng.GetBytes(bytes); // При переопределении в производном классе заполняет массив байтов криптостойкой случайной последовательностью значений.
            string hash = BitConverter.ToString(bytes).Replace("-", "").ToLower(); // убираем - и проебелы.
            return hash;
        }
    }
}
