﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.BlackBox
{
    internal class ConnectDB
    {
        public class ApplicationContext : DbContext
        {
            public DbSet<ConnectClass.TableLogin> TabLogin { get; set; }
            public DbSet<ConnectClass.TableLoginandStatus> TableLoginandStatus { get; set; }


            public ApplicationContext()
            {
                Database.EnsureCreated();
            }

            protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {
                optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=postgres;Username=postgres;Password=postgres");
            }
        }
    }
}
